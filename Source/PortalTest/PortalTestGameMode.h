// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "PortalTestGameMode.generated.h"

UCLASS(minimalapi)
class APortalTestGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	APortalTestGameMode();
};



