#include "HealthComponent.h"

UHealthComponent::UHealthComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
}

void UHealthComponent::BeginPlay()
{
	Super::BeginPlay();
}

void UHealthComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

float UHealthComponent::GetCurrentHealth()
{
	return Health;
}

void UHealthComponent::ChangeHealth(float ChangeValue)
{
	if (IsAlive)
	{
		Health += ChangeValue;

		OnHealthChange.Broadcast(Health, ChangeValue);

		if (Health <= 0.0f)
		{
			IsAlive = false;
			OnDead.Broadcast();
		}
	}
}

