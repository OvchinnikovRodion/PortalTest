// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class PortalTest : ModuleRules
{
	public PortalTest(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "EnhancedInput" });
	}
}
