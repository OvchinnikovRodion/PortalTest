// Copyright Epic Games, Inc. All Rights Reserved.

#include "PortalTestGameMode.h"
#include "PortalTestCharacter.h"
#include "UObject/ConstructorHelpers.h"

APortalTestGameMode::APortalTestGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPerson/Blueprints/Character/BP_ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
