#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "HealthComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnHealthChange, float, Health, float, Damage);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnDead);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class PORTALTEST_API UHealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	UHealthComponent();
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

UPROPERTY(BlueprintAssignable, Category = "Health")
	FOnHealthChange OnHealthChange;
UPROPERTY(BlueprintAssignable, Category = "Health")
	FOnDead OnDead;

UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Health")
	bool IsAlive = true;

UFUNCTION(BlueprintCallable, Category = "Health")
	float GetCurrentHealth();
UFUNCTION(BlueprintCallable, Category = "Health")
	virtual void ChangeHealth(float ChangeValue);

protected:
	virtual void BeginPlay() override;
	float Health = 100.0f;
};
